import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createStackNavigator, HeaderBackButton} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';
import HomeScreen from '../screens/HomeScreen';
import {Colors, Spacing, Typography} from '../styles';
import ProfileScreen from '../screens/ProfileScreen';

const Tab = createMaterialBottomTabNavigator();

const HomeStack = createStackNavigator();
const ProfileStack = createStackNavigator();

export default function BottomBar() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor={Colors.RED_PRIMARY}
      inactiveColor={Colors.GRAY_MEDIUM_PRIMARY}
      barStyle={{
        backgroundColor: Colors.WHITE,
        borderTopColor: Colors.GRAY_LIGHT_SECONDARY,
        borderTopWidth: 2,
      }}>
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color}) => (
            <Icon name="home-outline" color={color} size={Spacing.SCALE_22} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStackScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color}) => (
            <Icon name="person-outline" color={color} size={Spacing.SCALE_22} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
const HomeStackScreen = () => (
  <HomeStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: Colors.WHITE,
        borderBottomWidth: 2,
        borderBottomColor: Colors.GRAY_LIGHT_SECONDARY,
      },
      headerTitleStyle: {
        color: Colors.GRAY_DARK_SECONDARY,
        fontSize: Typography.LINE_HEIGHT_18,
        fontFamily: Typography.FONT_FAMILY_BOLD,
      },
    }}>
    <HomeStack.Screen
      name="Home"
      component={HomeScreen}
      options={{headerTitleAlign: 'center'}}
    />
  </HomeStack.Navigator>
);

const ProfileStackScreen = () => (
  <ProfileStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: Colors.WHITE,
        borderBottomWidth: 2,
        borderBottomColor: Colors.GRAY_LIGHT_SECONDARY,
      },
      headerTitleStyle: {
        color: Colors.GRAY_DARK_SECONDARY,
        fontSize: Typography.LINE_HEIGHT_18,
        fontFamily: Typography.FONT_FAMILY_BOLD,
      },
    }}>
    <ProfileStack.Screen
      name="Profile"
      component={ProfileScreen}
      options={{
        headerTitleAlign: 'center',
      }}
    />
  </ProfileStack.Navigator>
);
