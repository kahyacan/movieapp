import Axios from 'axios';
import React, {Component} from 'react';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableWithoutFeedback,
  Image,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import MovieLargeItem from '../components/MovieItem/MovieLargeItem';
import MovieSmallItem from '../components/MovieItem/MovieSmallItem';
import SearchBar from '../components/SearchBar';
import movieActions from '../redux/actions/MovieActions';
import {Colors, Mixins, Spacing, Typography} from '../styles';

export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;

const {fetchPopularMovie} = movieActions;

class HomeScreen extends Component {
  state = {
    grid: false,
    page: 1,
    search: false,
  };
  componentDidMount() {
    this.fetchMovie();
  }

  async fetchMovie() {
    await this.props.fetchPopularMovie(this.state.page);
  }

  changeGrid = () => {
    this.setState({
      grid: !this.state.grid,
    });
  };
  getMoreMovie = () => {
    this.setState(
      {
        page: this.state.page + 1,
      },
      () => {
        this.fetchMovie();
      },
    );
  };
  noItemDisplay = () => {
    return (
      <View style={{alignContent: 'center', justifyContent: 'center'}}>
        <Text style={[styles.titleText, {textAlign: 'center'}]}>
          Movie Not Found
        </Text>
      </View>
    );
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <SearchBar
          fetchPopularMovie={() => {
            this.props.fetchPopularMovie();
          }}
        />
        <View style={styles.titleContainer}>
          <Text style={styles.titleText}>Most popular</Text>
          <TouchableWithoutFeedback onPress={this.changeGrid}>
            <View>
              {this.state.grid ? (
                <Image
                  style={{width: Spacing.SCALE_25, height: Spacing.SCALE_25}}
                  source={{
                    uri:
                      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAE5JREFUOE9jLCgo+M9ARcBIMwP7+/spcmdhYSFYP9yFowaSHJ4YYUiyCTg0UD/ZUMtlMHMYqW4gzXIKtVw6mlPID0nalzbkuw1VJ9WzHgAdOEejzzDa9QAAAABJRU5ErkJggg==',
                  }}></Image>
              ) : (
                <Image
                  style={{width: Spacing.SCALE_25, height: Spacing.SCALE_25}}
                  source={{
                    uri:
                      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAF5JREFUOE9jLCgo+M9AAEyYMIERpIQYtYzEKCLLwP7+fgx3FhYWgsXQDcSnFu7CUQPBYUdRGOJLOWTFMtUMJJSoSZUH5wBqAtrlFKqFISwvj+YUynMK1cOQ6rFMLQMB2Geto0XnflAAAAAASUVORK5CYII=',
                  }}></Image>
              )}
            </View>
          </TouchableWithoutFeedback>
        </View>
        <FlatList
          data={this.props.movieList}
          style={styles.movieList}
          keyExtractor={(item, index) => {
            return index.toString();
          }}
          key={this.state.grid}
          numColumns={this.state.grid ? 2 : 1}
          onEndReached={this.props.search ? null : this.getMoreMovie}
          onEndReachedThreshold={0.5}
          ListEmptyComponent={this.noItemDisplay}
          renderItem={({item}) =>
            this.state.grid ? (
              <MovieSmallItem
                movie={item}
                navigation={this.props.navigation}></MovieSmallItem>
            ) : (
              <MovieLargeItem
                movie={item}
                navigation={this.props.navigation}></MovieLargeItem>
            )
          }></FlatList>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'space-around',
    marginTop: 0,
    backgroundColor: Colors.WHITE,
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: Spacing.SCALE_58,
    alignItems: 'center',
    paddingHorizontal: Spacing.SCALE_12,
  },
  titleText: {
    fontSize: Typography.FONT_SIZE_24,
    color: Colors.GRAY_DARK_SECONDARY,
    fontFamily: Typography.FONT_FAMILY_BOLD,
  },
  movieList: {},
});

const mapStateToProps = (state) => {
  const {movieList, search} = state.movie;
  return {movieList, search};
};

export default connect(mapStateToProps, {fetchPopularMovie})(HomeScreen);
