import React, {Component, useCallback} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  Text,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {deviceWidth} from '../components/SearchBar';
import movieActions from '../redux/actions/MovieActions';
import Icon from 'react-native-vector-icons/Ionicons';
import {deviceHeight} from './HomeScreen';
import {Colors, Mixins, Spacing, Typography} from '../styles';
import moment from 'moment';
import {set} from 'react-native-reanimated';

const {fetchMovieDetailById, fetchMovieCreditsById} = movieActions;

class DetailScreen extends Component {
  state = {
    movieLiked: false,
    loading: false,
    showMoreButton: false,
    textShown: false,
    numLines: undefined,
    movieCredits: {},
  };
  async componentDidMount() {
    if (this.props.route.params?.movieId) {
      const movieId = this.props.route.params.movieId;
      await this.props.fetchMovieDetailById(movieId);
      await this.props.fetchMovieCreditsById(movieId);
      this.setState({
        loading: true,
        movieLiked: this.props.route.params.movieLiked,
      });
    }
  }
  
  like = () => {
    this.setState({
      movieLiked: !this.state.movieLiked,
    });
    this.props.route.params.like();
  };
  timeFormat = () => {
    return moment
      .utc()
      .startOf('day')
      .add({minutes: this.props.movieDetail.runtime})
      .format('H[h] m[m]');
  };
  handleGenres = () => {
    return this.props.movieDetail.genres
      .slice(0, 2)
      .map((genre) => {
        return genre.name;
      })
      .join('/');
  };
  handleLangueage = () => {
    return this.props.movieDetail.spoken_languages[0].name;
  };
  toggleTextShown = () => {
    this.setState({
      textShown: !this.state.textShown,
      numLines: this.state.textShown ? 3 : undefined,
    });
  };
  onTextLayout = (e) => {
    if (e.nativeEvent.lines.length > 3 && !this.state.textShown) {
      this.setState({
        numLines: 3,
        showMoreButton: true,
      });
    }
  };
  castRender = () => {
    if (this.props.movieCredits != {}) {
      return (
        <View style={{flexDirection: 'row', marginRight: Spacing.SCALE_18}}>
          {this.props.movieCredits.cast?.slice(0, 3).map((cast, index) => {
            return (
              <View
                key={index.toString()}
                style={{
                  flexDirection: 'column',
                  marginRight: Spacing.SCALE_22,
                  alignItems: 'center',
                }}>
                <Text numberOfLines={1} style={styles.creditText}>
                  {cast.character}
                </Text>
                <Image
                  style={{
                    width: Spacing.SCALE_58,
                    height: Spacing.SCALE_58,
                    borderRadius: Spacing.SCALE_32,
                    borderColor: Colors.GRAY_DARK_PRIMARY,
                    borderWidth: 1,
                  }}
                  source={{
                    uri: cast.profile_path
                      ? 'https://image.tmdb.org/t/p/w500' + cast.profile_path
                      : `https://www.pngfind.com/pngs/m/676-6764065_default-profile-picture-transparent-hd-png-download.png`,
                  }}
                />
                <Text numberOfLines={1} style={styles.creditText}>
                  {cast.name}
                </Text>
              </View>
            );
          })}
        </View>
      );
    }
  };
  crewRender = () => {
    if (this.props.movieCredits) {
      return (
        <View style={{flexDirection: 'row', marginRight: Spacing.SCALE_18}}>
          {this.props.movieCredits.crew?.slice(0, 3).map((crew, index) => {
            return (
              <View
                key={index.toString()}
                style={{
                  flexDirection: 'column',
                  marginRight: Spacing.SCALE_22,
                  alignItems: 'center',
                }}>
                <Text numberOfLines={1} style={styles.creditText}>
                  {crew.name}
                </Text>
                <Image
                  style={{
                    width: Spacing.SCALE_58,
                    height: Spacing.SCALE_58,
                    borderRadius: Spacing.SCALE_32,
                    borderColor: Colors.GRAY_DARK_PRIMARY,
                    borderWidth: 1,
                  }}
                  source={{
                    uri: crew.profile_path
                      ? 'https://image.tmdb.org/t/p/w500' + crew.profile_path
                      : `https://www.pngfind.com/pngs/m/676-6764065_default-profile-picture-transparent-hd-png-download.png`,
                  }}
                />
                <Text numberOfLines={1} style={styles.creditText}>
                  {crew.job}
                </Text>
              </View>
            );
          })}
        </View>
      );
    }
  };
  render() {
    const {
      poster_path,
      runtime,
      genres,
      spoken_languages,
      overview,
    } = this.props.movieDetail;
    return this.state.loading ? (
      <ScrollView style={styles.container}>
        <View>
          <Image
            style={styles.image}
            source={{
              uri: 'https://image.tmdb.org/t/p/w500' + poster_path,
            }}
          />
          <TouchableWithoutFeedback onPress={() => this.like()}>
            <View
              style={[
                styles.likeView,
                {
                  backgroundColor: this.state.movieLiked
                    ? Colors.RED_PRIMARY
                    : Colors.GRAY_MEDIUM_SECONDARY,
                },
              ]}>
              <Icon
                name="heart"
                color={Colors.WHITE}
                size={deviceWidth * 0.1}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.contentContainer}>
          <View style={styles.movieInfoView}>
            <View>
              <Text style={styles.title}>Duration</Text>
              <Text style={styles.text}>
                {runtime ? this.timeFormat() : 'Null'}
              </Text>
            </View>
            <View>
              <Text style={styles.title}>Genre</Text>
              <Text
                style={[
                  styles.text,
                  {
                    width: Spacing.SCALE_80,
                  },
                ]}>
                {genres ? this.handleGenres() : null}
              </Text>
            </View>
            <View>
              <Text style={styles.title}>Language</Text>
              <Text style={styles.text}>
                {spoken_languages ? this.handleLangueage() : null}
              </Text>
            </View>
          </View>
          <View style={styles.synopsisView}>
            <Text style={styles.title}>Synopsis</Text>
            <Text
              onTextLayout={(e) => {
                this.onTextLayout(e);
              }}
              numberOfLines={this.state.numLines}
              style={styles.text}
              ellipsizeMode="tail">
              {overview}
            </Text>
            {this.state.showMoreButton ? (
              <Text
                onPress={this.toggleTextShown}
                style={{
                  color: Colors.RED_SECONDARY,
                  fontFamily: Typography.FONT_FAMILY_BOLD,
                  fontSize: Typography.FONT_SIZE_14,
                  alignSelf: 'flex-end',
                }}>
                {this.state.textShown ? 'Read Less' : 'Read More'}
              </Text>
            ) : null}
          </View>
          <View style={styles.castView}>
            <Text style={styles.title}>Main Cast</Text>
            {this.castRender()}
          </View>
          <View>
            <Text style={styles.title}>Main technical team</Text>
            {this.crewRender()}
          </View>
        </View>
      </ScrollView>
    ) : null;
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.WHITE,
    flex: 1,
    flexDirection: 'column',
  },
  contentContainer: {
    flexDirection: 'column',
    marginVertical: Spacing.SCALE_32,
    marginHorizontal: Spacing.SCALE_25,
  },
  likeView: {
    position: 'absolute',
    width: Spacing.SCALE_58,
    height: Spacing.SCALE_58,
    borderRadius: Spacing.SCALE_32,
    right: Spacing.SCALE_32,
    bottom: -Spacing.SCALE_25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: Spacing.SCALE_200,
    resizeMode: 'cover',
  },
  movieInfoView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: Spacing.SCALE_16,
  },
  title: {
    color: Colors.GRAY_DARK_SECONDARY,
    fontSize: Typography.FONT_SIZE_19,
    fontFamily: Typography.FONT_FAMILY_BOLD,
    marginBottom: Spacing.SCALE_4,
  },
  text: {
    color: Colors.GRAY_DARK_SECONDARY,
    fontSize: Typography.FONT_SIZE_16,
    fontFamily: Typography.FONT_FAMILY_MEDIUM,
  },
  synopsisView: {
    marginBottom: Spacing.SCALE_16,
  },
  castView: {
    marginBottom: Spacing.SCALE_16,
  },
  creditText: {
    color: Colors.GRAY_DARK_SECONDARY,
    fontSize: Typography.FONT_SIZE_14,
    fontFamily: Typography.FONT_FAMILY_MEDIUM,
    marginVertical: Spacing.SCALE_8,
    width: Mixins.WINDOW_WIDTH * 0.25,
    textAlign: 'center',
  },
});

const mapStateToProps = (state) => {
  const {movieDetail, movieCredits} = state.movie;
  return {movieDetail, movieCredits};
};
export default connect(mapStateToProps, {
  fetchMovieDetailById,
  fetchMovieCreditsById,
})(DetailScreen);
