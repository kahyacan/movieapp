import React from 'react';
import {View, Text} from 'react-native';
import {Typography} from '../styles';

export default function ProfileScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignContent: 'center'}}>
      <Text
        style={{
          fontSize: Typography.FONT_SIZE_16,
          fontFamily: 'SansitaSwashed-Bold',
          textAlign: 'center',
        }}>
        This is Your Profile Page
      </Text>
      <Text
        style={{
          fontSize: Typography.FONT_SIZE_40,
          fontFamily: 'SansitaSwashed-Bold',
          textAlign: 'center',
        }}>
        Coming Soon....
      </Text>
    </View>
  );
}
