import React, {Component} from 'react';
import moment from 'moment';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {deviceWidth} from '../SearchBar';
import Icon from 'react-native-vector-icons/Ionicons';
import {deviceHeight} from '../../screens/HomeScreen';
import {Colors, Spacing, Typography} from '../../styles';
import {FONT_FAMILY_MEDIUM, FONT_WEIGHT_MEDIUM} from '../../styles/typography';

class MovieSmallItem extends Component {
  genres = [
    {
      id: 28,
      name: 'Action',
    },
    {
      id: 12,
      name: 'Adventure',
    },
    {
      id: 16,
      name: 'Animation',
    },
    {
      id: 35,
      name: 'Comedy',
    },
    {
      id: 80,
      name: 'Crime',
    },
    {
      id: 99,
      name: 'Documentary',
    },
    {
      id: 18,
      name: 'Drama',
    },
    {
      id: 10751,
      name: 'Family',
    },
    {
      id: 14,
      name: 'Fantasy',
    },
    {
      id: 36,
      name: 'History',
    },
    {
      id: 27,
      name: 'Horror',
    },
    {
      id: 10402,
      name: 'Music',
    },
    {
      id: 9648,
      name: 'Mystery',
    },
    {
      id: 10749,
      name: 'Romance',
    },
    {
      id: 878,
      name: 'Science Fiction',
    },
    {
      id: 10770,
      name: 'TV Movie',
    },
    {
      id: 53,
      name: 'Thriller',
    },
    {
      id: 10752,
      name: 'War',
    },
    {
      id: 37,
      name: 'Western',
    },
  ];
  state = {
    movieLiked: false,
  };
  getGenreName = (arr) => {
    return arr.map((id) => {
      return this.genres.find((genre) => {
        return genre.id == id;
      }).name;
    });
  };
  like = () => {
    this.setState({
      movieLiked: !this.state.movieLiked,
    });
  };

  detailScreen = (id) => {
    this.props.navigation.navigate('Detail', {
      movieId: id,
      movieLiked: this.state.movieLiked,
      like: () => {
        this.like();
      },
    });
  };
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.detailScreen(this.props.movie.id)}>
        <View style={styles.container}>
          <View>
            <Image
              style={styles.image}
              source={{
                uri: this.props.movie.poster_path
                  ? 'https://image.tmdb.org/t/p/w500' +
                    this.props.movie.poster_path
                  :"https://valmorgan.co.nz/wp-content/uploads/2016/06/default-movie-1-3.jpg"}}
            />
            <Image
              style={styles.gravityImage}
              source={{
                uri:
                  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALgAAAB2CAYAAAB70VkDAAAABHNCSVQICAgIfAhkiAAABkhJREFUeF7tnEfInGUYRZ9YyEYUXLixEGNXLFiwK4o9Yq9YsEUlKhYiFjRiDNZgwShqbETFXrFX7Ioa7EajUSwbFy5EF8ESeeHPRjT3zjj57s94Znu/uXe+M4fhncXMmOIBgSEmMGaI741bg0AhOBIMNQEEH+q3l5tDcBwYagIIPtRvLzc3aMEH3cc79P8ksGBQt92vkO15S1TxJXVQbwQ9iyTQhP+zqnoWvx/Bm9hLIjdKdkygyf3HiOj2dK+CL1VVS9vtXAiBwRP4rap+d2t7Ebx9ao/lk9tFy3WLiUD7JJ8/8mkuJ3oRfJmRc7cs5QIILGYC7Tz+i7PhCt6OJU1wHhAYLQSa4O24ssiHK3iTux1PeEBgtBBoxxT5Ke4KvnxVtS+YPCAwWgi0L5o/qRfjCr4C52+FkrxjAu0c/qPadAVfURWRQyBA4Ae16Qq+sioih0CAwHdq0xV8nCoih0CAwDdq0xV8vCoih0CAwDy16Qq+hioih0CAwFy16Qq+tioih0CAwBy16Qq+rioih0CAwKdq0xV8fVVEDoEAgY/Upiv4hqqIHAIBAh+oTVfwjVUROQQCBGarTVfwTVUROQQCBN5Vm67gm6sicggECLytNl3Bt1RF5BAIEHhTbbqCb6OKyCEQIPCa2nQF304VkUMgQOAVtekKvoMqIodAgMBLatMVfCdVRA6BAIHn1aYr+C6qiBwCAQLPqk1X8N1VETkEAgSeUpuu4BNUETkEAgSeUJuu4HupInIIBAg8pjZdwfdRReQQCBB4RG26gu+visghECDwoNp0BT9QFZFDIEDgfrXpCn6IKiKHQIDAPWrTFfwwVUQOgQCBu9SmK/gRqogcAgECd6hNV/CjVBE5BAIEblebruDHqCJyCAQI3Ko2XcEnqiJyCAQIzFSbruAnqCJyCAQI3Kg2XcEnqSJyCAQIXK82XcFPVkXkEAgQmKE2XcFPVUXkEAgQuEZtuoKfrorIIRAgcJXadAWfrIrIIRAgMF1tuoKfpYrIIRAgcJnadAU/RxWRQyBA4BK16Qp+nioih0CAwDS16Qo+RRWRQyBAYKradAW/UBWRQyBA4AK16Qp+kSoih0CAwPlq0xX8YlVEDoEAgXPVpiv4paqIHAIBAmerTVfwK1QROQQCBM5Um67gV6oicggECJyhNl3Br1ZF5BAIEDhNbbqCX6uKyCEQIHCK2nQFv04VkUMgQOAktekKfoMqIodAgMCJatMV/CZVRA6BAIHj1aYr+C2qiBwCAQLHqk1X8NtUETkEAgSOVpuu4LNUETkEAgSOVJuu4HeqInIIBAgcrjZdwe9WReQQCBA4VG26gt+nisghECBwkNp0BX9AFZFDIEDgALXpCv6wKiKHQIDAvmrTFfxRVUQOgQCBvdWmK/jjqogcAgECe6pNV/AnVRE5BAIE9lCbruDPqCJyCAQI7Ko2XcGfU0XkEAgQ2FltuoK/qIrIIRAgsKPadAV/WRWRQyBAYHu16Qr+qioih0CAwLZq0xX8DVVEDoEAga3Upiv4W6qIHAIBAluoTVfwd1QROQQCBDZTm67g76kicggECGyiNl3B31dF5BAIENhIbbqCf6iKyCEQILCB2nQF/0QVkUMgQGA9tekK/pkqIodAgMA6atMV/AtVRA6BAIE11aYr+FeqiBwCAQKrqU1X8K9VETkEAgRWVZuu4N+qInIIBAisojZdwb9XReQQCBBYSW26gn9eVcuqMnIIdEjg56paS+25gr9eVaurMnIIdEjgy6raWu25gs+sKvkfFGqMHAIDJND+q2ei6nMF36+qblZl5BDokMBxVfWQ2nMFH1tVs6tKHurVIDkEBkBg7sjxZL7qcgVvPbtVFf9RqIiSd0FgQlVZvxPuRfD2widX1dQu7oANCPwLgSlVNd2l06vgCyWfVlX9PNd9XVwHgb8TWFBVTe7Le0HTr6TtD1dmVNX4Xsa4FgJ9EphXVZOq6oVen9+v4At3Dh45m7dfVoyrquV6fQFcD4F/IPBrVc2pqo+r6umqurdfSv9V8H53eR4EOiGA4J1gZiRFAMFT5NnthACCd4KZkRQBBE+RZ7cTAgjeCWZGUgQQPEWe3U4IIHgnmBlJEUDwFHl2OyGA4J1gZiRFAMFT5NnthACCd4KZkRSBvwAgi4B38+STwQAAAABJRU5ErkJggg==',
              }}></Image>
            <Text numberOfLines={3} style={styles.movieTittle}>
              {this.props.movie.original_title}
            </Text>
            <TouchableWithoutFeedback onPress={() => this.like()}>
              <View style={styles.likeView}>
                <Icon
                  name={this.state.movieLiked ? 'heart' : 'heart-outline'}
                  color={
                    this.state.movieLiked ? Colors.RED_PRIMARY : Colors.WHITE
                  }
                  size={Typography.LINE_HEIGHT_18}
                />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.childContainer}>
            <View>
              <Text style={styles.movieText}>
                {moment(this.props.movie.release_date).format('YYYY')} |{' '}
                {this.props.movie.original_language}
              </Text>
              <Text style={styles.movieText}>
                {this.getGenreName(this.props.movie.genre_ids.slice(0, 2)).join(
                  ' / ',
                )}
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: Spacing.SCALE_12,
  },
  childContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingHorizontal: Spacing.SCALE_8,
    marginTop: Spacing.SCALE_8,
    width: deviceWidth * 0.38,
  },
  gravityImage: {
    width: Spacing.SCALE_183,
    height: Spacing.SCALE_110,
    left: -Spacing.SCALE_9,
    bottom: -Spacing.SCALE_12,
    position: 'absolute',
    resizeMode: 'cover',
  },
  movieTittle: {
    fontSize: Typography.FONT_SIZE_19,
    fontFamily: Typography.FONT_FAMILY_BOLD,
    color: Colors.WHITE,
    position: 'absolute',
    left: Spacing.SCALE_8,
    bottom: Spacing.SCALE_8,
    width: deviceWidth * 0.38,
  },
  movieText: {
    fontSize: Typography.FONT_SIZE_16,
    fontFamily: FONT_FAMILY_MEDIUM,
    color: Colors.GRAY_MEDIUM_SECONDARY,
    textTransform: 'capitalize',
  },
  likeView: {
    position: 'absolute',
    width: deviceWidth * 0.07,
    height: deviceWidth * 0.07,
    borderRadius: deviceWidth * 0.05,
    left: deviceWidth * 0.35,
    top: deviceWidth * 0.02,
    borderColor: Colors.WHITE,
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: deviceWidth * 0.44,
    height: deviceWidth * 0.55,
    borderRadius: deviceWidth * 0.025,
  },
});

export default MovieSmallItem;
