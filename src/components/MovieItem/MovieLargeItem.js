import React, {Component} from 'react';
import moment from 'moment';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {deviceWidth} from '../SearchBar';
import Icon from 'react-native-vector-icons/Ionicons';
import {Colors, Spacing, Typography} from '../../styles';
import {FONT_SIZE_16} from '../../styles/typography';

class MovieItem extends Component {
  genres = [
    {
      id: 28,
      name: 'Action',
    },
    {
      id: 12,
      name: 'Adventure',
    },
    {
      id: 16,
      name: 'Animation',
    },
    {
      id: 35,
      name: 'Comedy',
    },
    {
      id: 80,
      name: 'Crime',
    },
    {
      id: 99,
      name: 'Documentary',
    },
    {
      id: 18,
      name: 'Drama',
    },
    {
      id: 10751,
      name: 'Family',
    },
    {
      id: 14,
      name: 'Fantasy',
    },
    {
      id: 36,
      name: 'History',
    },
    {
      id: 27,
      name: 'Horror',
    },
    {
      id: 10402,
      name: 'Music',
    },
    {
      id: 9648,
      name: 'Mystery',
    },
    {
      id: 10749,
      name: 'Romance',
    },
    {
      id: 878,
      name: 'Science Fiction',
    },
    {
      id: 10770,
      name: 'TV Movie',
    },
    {
      id: 53,
      name: 'Thriller',
    },
    {
      id: 10752,
      name: 'War',
    },
    {
      id: 37,
      name: 'Western',
    },
  ];
  state = {
    movieLiked: false,
  };
  getGenreName = (arr) => {
    return arr.map((id) => {
      return this.genres.find((genre) => {
        return genre.id == id;
      }).name;
    });
  };
  like = () => {
    this.setState({
      movieLiked: !this.state.movieLiked,
    });
  };

  detailScreen = (id) => {
    this.props.navigation.push('Detail', {
      movieId: id,
      movieLiked: this.state.movieLiked,
      like: () => {
        this.like();
      },
    });
  };
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.detailScreen(this.props.movie.id)}>
        <View style={styles.container}>
          <Image
            style={styles.image}
            source={{
              uri: this.props.movie.poster_path
                ? 'https://image.tmdb.org/t/p/w500' +
                  this.props.movie.poster_path
                : 'https://valmorgan.co.nz/wp-content/uploads/2016/06/default-movie-1-3.jpg',
            }}
          />
          <TouchableWithoutFeedback onPress={() => this.like()}>
            <View style={styles.likeView}>
              <Icon
                name={this.state.movieLiked ? 'heart' : 'heart-outline'}
                color={
                  this.state.movieLiked ? Colors.RED_PRIMARY : Colors.WHITE
                }
                size={deviceWidth * 0.05}
              />
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.childContainer}>
            <View>
              <Text numberOfLines={3} style={styles.movieTittle}>
                {this.props.movie.original_title}
              </Text>
              <Text style={styles.movieText}>
                {moment(this.props.movie.release_date).format('YYYY')} |{' '}
                {this.props.movie.original_language}
              </Text>
              <Text style={styles.movieText}>
                {this.getGenreName(this.props.movie.genre_ids.slice(0, 2)).join(
                  ' / ',
                )}
              </Text>
            </View>
            <View>
              <Text style={styles.movieText}>
                {this.props.movie.vote_average}
              </Text>
              <Text style={styles.movieText}>
                Public {/*Buraya ne geleceğini bulamadım :)*/}
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    margin: Spacing.SCALE_12,
  },
  childContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginLeft: Spacing.SCALE_18,
  },
  movieTittle: {
    fontSize: Typography.FONT_SIZE_19,
    fontFamily: Typography.FONT_FAMILY_BOLD,
    color: Colors.GRAY_DARK_SECONDARY,
  },
  movieText: {
    fontSize: 18,
    fontFamily: Typography.FONT_FAMILY_MEDIUM,
    color: Colors.GRAY_MEDIUM_SECONDARY,
    textTransform: 'capitalize',
  },
  likeView: {
    position: 'absolute',
    width: deviceWidth * 0.07,
    height: deviceWidth * 0.07,
    borderRadius: deviceWidth * 0.05,
    left: Spacing.SCALE_110,
    top: Spacing.SCALE_12,
    borderColor: Colors.WHITE,
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: Spacing.SCALE_150,
    height: Spacing.SCALE_200,
    borderRadius: deviceWidth * 0.025,
  },
});

export default MovieItem;
