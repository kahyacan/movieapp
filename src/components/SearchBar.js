import {min} from 'moment';
import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {connect} from 'react-redux';
import movieActions from '../redux/actions/MovieActions';
import {Mixins, Spacing, Typography} from '../styles/index';
export const deviceWidth = Dimensions.get('window').width;

const {searchMovieByName} = movieActions;

class SearchBar extends Component {
  state = {
    searchText: '',
  };
  onChangeText = (text) => {
    if (text == '') {
      this.props.fetchPopularMovie();
    } else {
      this.setState(
        {
          searchText: text,
        },
        () => {
          this.props.searchMovieByName(text);
        },
      );
    }
  };
  render() {
    return (
      <View style={styles.inputContainer}>
        <Icon
          style={styles.prefix}
          name="search"
          color={'#9C9DA1'}
          size={Spacing.SCALE_22}
        />
        <TextInput
          placeholder="Search"
          style={styles.searchText}
          placeholderTextColor={'#9C9DA1'}
          onChangeText={(text) => this.onChangeText(text)}
          value={this.state.value}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    marginTop: 12,
    backgroundColor: '#E4E4E4',
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    paddingRight: Spacing.SCALE_18,
    borderRadius: Spacing.SCALE_22,
    height: Spacing.SCALE_32,
  },
  prefix: {
    paddingLeft: Spacing.SCALE_8,
  },
  searchText: {
    width: Mixins.WINDOW_WIDTH * 0.75,
    fontSize: Typography.FONT_SIZE_16,
    fontFamily: Typography.FONT_FAMILY_BOLD,
    paddingTop: 0,
    paddingBottom: 0,
  },
});

export default connect(null, {searchMovieByName})(SearchBar);
