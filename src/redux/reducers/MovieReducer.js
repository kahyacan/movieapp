import {act} from 'react-test-renderer';
import movieActions from '../actions/MovieActions';

const initialState = {
  movieList: [],
  movieDetail: {},
  movieCredits: {},
  search: false,
};
export const movieReducer = function (state = initialState, action) {
  switch (action.type) {
    case movieActions.FETCH_MOVIE_SUCCESS:
      return {
        ...state,
        search: false,
        movieList: state.search
          ? action.payload
          : [...state.movieList, ...action.payload],
      };
    case movieActions.MOVIE_FETCH_BY_ID_SUCCESS:
      return {
        ...state,
        movieDetail: action.payload,
      };
    case movieActions.MOVIE_CREDIT_FETCH_SUCCESS:
      return {
        ...state,
        movieCredits: action.payload,
      };
    case movieActions.SEARCH_MOVIE_SUCCESS:
      return {
        ...state,
        search: true,
        movieList: action.payload,
      };
    default:
      return state;
  }
};
