import Axios from 'axios';
import {act} from 'react-test-renderer';

const BASE_URL = 'https://api.themoviedb.org/3/';
const API_KEY = '579c7fc7840ab037036071939351dc60';

const movieActions = {
  FETCH_MOVIE_SUCCESS: 'fetch movie success',
  FETCH_MOVIE_FAIL: 'fetch movie fail',
  MOVIE_FETCH_BY_ID_SUCCESS: 'fetch movie by id success',
  MOVIE_CREDIT_FETCH_SUCCESS: 'fetch movie credit by id success',
  SEARCH_MOVIE_SUCCESS: 'search movie success',
  REQUEST_SEARCH_MOVIE: 'request search movie',
  deneme: 'deneme',
  fetchPopularMovie: (page = 1) => {
    return (dispatch) => {
      Axios.get(BASE_URL + 'movie/popular?api_key=' + API_KEY, {
        params: {
          page,
        },
      }).then((res) => {
        movieActions.movieFetchSuccess(dispatch, res.data.results);
      });
    };
  },
  fetchMovieDetailById: (movieId) => {
    return (dispatch) => {
      Axios.get(BASE_URL + 'movie/' + movieId + '?api_key=' + API_KEY).then(
        (res) => {
          movieActions.movieFetchByIdSuccess(dispatch, res.data);
        },
      );
    };
  },
  fetchMovieCreditsById: (movieId) => {
    return (dispatch) => {
      Axios.get(
        BASE_URL + 'movie/' + movieId + '/credits?api_key=' + API_KEY,
      ).then((res) => {
        movieActions.movieCreditsFetchSuccess(dispatch, res.data);
      });
    };
  },
  searchMovieByName: (query, page = 1) => {
    return (dispatch) => {
      Axios.get(BASE_URL + 'search/movie?api_key=' + API_KEY, {
        params: {
          query,
          page,
        },
      }).then((res) => {
        movieActions.searchMovieSuccess(dispatch, res.data.results);
      });
    };
  },
  searchMovieSuccess: (dispatch, movies) => {
    return dispatch({
      type: movieActions.SEARCH_MOVIE_SUCCESS,
      payload: movies,
    });
  },
  movieCreditsFetchSuccess: (dispatch, credits) => {
    return dispatch({
      type: movieActions.MOVIE_CREDIT_FETCH_SUCCESS,
      payload: credits,
    });
  },
  movieFetchByIdSuccess: (dispatch, movie) => {
    return dispatch({
      type: movieActions.MOVIE_FETCH_BY_ID_SUCCESS,
      payload: movie,
    });
  },
  movieFetchSuccess: (dispatch, data) => {
    return dispatch({
      type: movieActions.FETCH_MOVIE_SUCCESS,
      payload: data,
    });
  },
};

export default movieActions;
