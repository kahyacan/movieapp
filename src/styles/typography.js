import {scaleFont} from './mixins';

// FONT FAMILY
export const FONT_FAMILY_MEDIUM = 'WorkSans-Medium';
export const FONT_FAMILY_BOLD = 'WorkSans-SemiBold';

// FONT WEIGHT
export const FONT_WEIGHT_MEDIUM = '400';
export const FONT_WEIGHT_BOLD = '700';

// FONT SIZE
export const FONT_SIZE_40 = scaleFont(40);
export const FONT_SIZE_24 = scaleFont(24);
export const FONT_SIZE_19 = scaleFont(19);
export const FONT_SIZE_16 = scaleFont(16);
export const FONT_SIZE_14 = scaleFont(14);
export const FONT_SIZE_12 = scaleFont(12);

// LINE HEIGHT
export const LINE_HEIGHT_24 = scaleFont(24);
export const LINE_HEIGHT_20 = scaleFont(20);
export const LINE_HEIGHT_18 = scaleFont(18);
export const LINE_HEIGHT_16 = scaleFont(16);

// FONT STYLE
export const FONT_MEDIUM = {
  fontFamily: FONT_FAMILY_MEDIUM,
  fontWeight: FONT_WEIGHT_MEDIUM,
};

export const FONT_BOLD = {
  fontFamily: FONT_FAMILY_BOLD,
  fontWeight: FONT_WEIGHT_BOLD,
};
