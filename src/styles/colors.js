export const WHITE = '#FFFFFF';
export const BLACK = '#000000';

//REDSCALE
export const RED_PRIMARY = '#FF0000';
export const RED_SECONDARY = '#FF354A';

// GRAYSCALE
export const GRAY_LIGHT_PRIMARY = '#E4E4E4';
export const GRAY_LIGHT_SECONDARY = '#DBDBDB';
export const GRAY_LIGHT_TERTIARY = '#CBCBCB';

export const GRAY_MEDIUM_PRIMARY = '#A3A6AB';
export const GRAY_MEDIUM_SECONDARY = '#9C9DA1';

export const GRAY_DARK_PRIMARY = '#707070';
export const GRAY_DARK_SECONDARY = '#575757';
