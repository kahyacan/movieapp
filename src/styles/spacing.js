import {scaleSize} from './mixins';

export const SCALE_4 = scaleSize(4);
export const SCALE_8 = scaleSize(8);
export const SCALE_9 = scaleSize(9);
export const SCALE_12 = scaleSize(12);
export const SCALE_16 = scaleSize(16);
export const SCALE_18 = scaleSize(18);
export const SCALE_22 = scaleSize(22);
export const SCALE_25 = scaleSize(25);
export const SCALE_32 = scaleSize(32);
export const SCALE_48 = scaleSize(48);
export const SCALE_58 = scaleSize(58);
export const SCALE_80 = scaleSize(80);
export const SCALE_110 = scaleSize(110);
export const SCALE_150 = scaleSize(150);
export const SCALE_183 = scaleSize(183);
export const SCALE_200 = scaleSize(200);
