import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import BottomBar from './src/commons/BottomBar';
import ProfileScreen from './src/screens/ProfileScreen';
import {Provider} from 'react-redux';
import {store} from './src/redux/store';
import DetailScreen from './src/screens/DetailScreen';
import {Colors, Typography} from './src/styles';
const Stack = createStackNavigator();
const App = () => {
  return (
    <Provider store={store}>
      <StatusBar hidden />
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerTintColor: Colors.GRAY_DARK_SECONDARY,
          }}>
          <Stack.Screen
            name="Home"
            component={BottomBar}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Detail"
            component={DetailScreen}
            options={{
              headerTitleAlign: 'center',
              headerTitleStyle: {
                color: Colors.GRAY_DARK_SECONDARY,
                fontSize: Typography.LINE_HEIGHT_18,
                fontFamily: Typography.FONT_FAMILY_BOLD,
              },
              headerBackTitleVisible: true,
              headerBackTitleStyle: {
                color: Colors.GRAY_DARK_SECONDARY,
                fontSize: Typography.LINE_HEIGHT_18,
                fontFamily: Typography.FONT_FAMILY_BOLD,
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
